const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const pug = require('gulp-pug');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass')(require('sass'));
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');

const PUBLIC_DIR = './public';

// Sources
const scssSrcDir = './src/scss';
const jsSrcDir = './src/js';

// Destination
const cssDestDir = `${PUBLIC_DIR}/css`;
const jsDestDir = `${PUBLIC_DIR}/js`;

// Watch files
const pugWatchFiles = './src/templates/**/*.pug';
const scssWatchFiles = scssSrcDir + '/**/*.scss';

function pugToHtml() {
  return gulp
    .src('./src/templates/pages/*.pug')
    .pipe(
      pug({
        pretty: false,
      })
    )
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(PUBLIC_DIR));
}

function css() {
  return gulp
    .src(`${scssSrcDir}/main.scss`, { sourcemaps: true })
    .pipe(
      sass({
        // outputStyle: 'compressed',
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest(cssDestDir, { sourcemaps: '.' }));
}

function javascript() {
  return (
    gulp
      .src(`${jsSrcDir}/main.js`)
      .pipe(plumber())
      .pipe(
        babel({
          plugins: ['@babel/transform-runtime'],
        })
      )
      // .pipe(sourcemaps.write())
      .pipe(gulp.dest(jsDestDir))
  );
}

function watch() {
  browserSync.init({
    open: 'external',
    proxy: 'http://localhost/1-PROJECTS/JavaScript/Calculator/public',
  });
  gulp.watch(pugWatchFiles, pugToHtml);
  gulp.watch(scssWatchFiles, css);
  gulp.watch(jsSrcDir, javascript);
  gulp
    .watch([pugWatchFiles, jsDestDir + '/main.js', cssDestDir + '/main.css'])
    .on('change', reload);
}

async function build() {
  pugToHtml();
  css();
  javascript();
}

gulp.task('default', watch);
gulp.task('watch', watch);
gulp.task('build', build);
